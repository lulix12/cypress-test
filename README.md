# Welcome
Thank you for taking your time in looking into this code. The tests are written in Cypress and are testing a few scenarios related to the "labels" creation in the "Folders-Labels"  section of the the settings located at https://account.protonmail.com/u/69/mail/folders-labels.

In summary:
- The tests are running with the latest version of Cypress https://www.cypress.io/
- The tests are running against the "account" app from the public Github repo https://github.com/ProtonMail/WebClients
- The test cases are in the `cypress/e2e` folder
- The scenarios tested are limited to 'CRUD' (create, read, delete and update labels)

## Installation
- Firstly you will need to run the application. For the purpose of the task I used the publicly available app from the Github web clients https://github.com/ProtonMail/WebClients:

```sh
    git clone https://github.com/ProtonMail/WebClients
    cd WebClients
    yarn install
    yarn workspace proton-account start
```

- To run the tests, open a separate tab on your terminal while `proton-account` is running:
```sh
    git clone https://gitlab.com/lulix12/cypress-test.git
    npm install
    npx cypress run
```

- Alternately you can use the Cypress runner to select the tests individually:
```sh
    npx cypress open
```
## Linting
The framework uses ESlint to ensure consistent linting rules are followed throughout. To run a lint check run:
```sh
  npm run lint
```

## Pipeline
The repository has a very simple pipeline setup which at the moment is not going to run because is on a free account trial. The idea is to run lint checks, build the app and then run the Cypress tests when the app is build

## Reporting
The framework uses Mochawesome reports, to generate a report add this flag when you run the tests:
```sh
  npx run cypress --reporter mochawesome
```
At the end of the test run an HTML link will be generated.

## Additional notes
- I left the comments in the code to make it more readable for the sake of this exercise
- Some of the challenges I faced during the tasks are related to running the Proton Account web-app locally, specifically:
    - Initially I thought of using the production version of the site however I did hit a roadblock there because production wouldn't render when using Cypress. I believe this might be due to security reasons.
    - The workaround for above has been using the open source version of the app, which in itself came with some limitations
    - Firstly, there is a "soft" block on the numbers of logins you can make it
    - Secondly, some app errors were triggered during the actual tests execution, especially in the scenarios where testing the negative paths such as creating 2 labels with the same name
    - Having said that, the main issue I had was related to not being able to fully access and modify the state of the app. For instance I had to write a custom command to login via the GUI, as well as other commands such as creating and deleting labels via the GUI. I do think some of the scenarios I wrote here (if not the majority of them) would be fit for tests at a "lower level" such as integration or API tests.
