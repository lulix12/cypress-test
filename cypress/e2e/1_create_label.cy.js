describe("Create and delete a label", () => {
  beforeEach(() => {
    /*
        This step is reused in all of the scenarios 
        ideally it should be mocked via the API unless 
        you want to test the GUI
        */
    cy.login("cypress-test-12", "Qy5vgj+YdPpP!%L");
  });

  it("Should allow to create a new label and also delete it", () => {
    /* 
        I am putting creation and deletion together because I can't fully 
        control the state of the application and without a "cleanup" the 
        tests can become flaky
        */
    cy.create_label("test-label");
    cy.delete_label();
  });

  it.skip("Should not allow to create 2 labels with identical names", () => {
    cy.create_label("test-label");
    cy.create_label("test-label");
    cy.on("uncaught:exception", () => {
      cy.get(".p1").should(
        "have.text",
        "A label or a folder with this name already exists"
      );
      /* this could be better; the application throws an error when trying to create 2 labels 
            when using https://github.com/ProtonMail/WebClients.
            The assumption for this case is that if an exception error is thrown than the user is not
            able to add 2 labels with the same name.
            */
    });
  });

  it.skip("Should be able to create up to 3 labels when on the free plan", () => {
    /* To enable this scenario the `create_label` custom step
        needs some refactoring to handle multiple lists (i.e the 
         assertion on line 42 in cypress/support/commands.js will fail)
         (ran out of time)
        */
  });
});
