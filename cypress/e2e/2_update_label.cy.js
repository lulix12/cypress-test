describe("A user can update a label", () => {
  beforeEach(() => {
    /* the login details could be in fixture or 
        set as environment variables ideally but for the sake
        of this exercise I left them "exposed" */
    cy.login("cypress-test-12", "Qy5vgj+YdPpP!%L");
    cy.create_label("test-label");
  });

  it("Updates a label", () => {
    cy.update_label("new-label");
    cy.delete_label();
  });
});
