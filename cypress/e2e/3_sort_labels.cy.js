describe("A user can update a label", () => {
  beforeEach(() => {
    cy.login("cypress-test-12", "Qy5vgj+YdPpP!%L");
  });

  it("The sort label should not be visible if there are no labels", () => {
    cy.get(
      '[data-target-id="labellist"] > .max-w46e > .mb2 > .button-outline-weak'
    ).should("not.exist");
  });

  it("The sort button should be visible if there are labels", () => {
    //this could be mocked instead of having to create labels "manually"
    cy.create_label("test-label");
    cy.get(
      '[data-target-id="labellist"] > .max-w46e > .mb2 > .button-outline-weak'
    )
      .scrollIntoView()
      .should("be.visible");
  });

  it("I should be able to sort the labels", () => {
    cy.get(
      '[data-target-id="labellist"] > .max-w46e > .mb2 > .button-outline-weak'
    )
      .scrollIntoView()
      .click();
    cy.on("window:alert", (str) => {
      cy.get(".notifications-container").should("have.text", "Labels sorted");
    });
    cy.delete_label();
  });

  it.skip("the labels are draggable", () => {});
});
