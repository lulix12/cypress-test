// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })

Cypress.Commands.add("login", (username, password) => {
  cy.visit("/mail/folders-labels");
  cy.get("#username").type(username);
  cy.get("#password").type(password);
  cy.get("form > .button").click();
  cy.get("h1")
    .should("be.visible", { timeout: 20000 })
    .and("have.text", "Folders and labels");
});

Cypress.Commands.add("create_label", (label_name) => {
  cy.get('[data-target-id="labellist"] > .max-w46e > .mb2 > .button')
    .first()
    .scrollIntoView()
    .click();
  cy.get('[data-test-id="label/folder-modal:name"]').type(label_name);
  cy.get('.field-container > [data-testid="dropdown-button"]').click();
  cy.get(".color-selector-item").first().click();
  cy.get(".modal-two-footer > .button-solid-norm").click();
  cy.get('[data-test-id="folders/labels:item-name"]')
    .scrollIntoView()
    .should("have.text", label_name);
});

Cypress.Commands.add("delete_label", () => {
  cy.get(
    '[data-test-id="folders/labels:item-type:label"] [data-test-id="dropdown:open"]'
  )
    .first()
    .scrollIntoView()
    .click();
  cy.get('[data-test-id="folders/labels:item-delete"]').click();
  cy.get(".button-solid-danger").click({ force: true });
});

Cypress.Commands.add("update_label", (new_label_name) => {
  cy.get(
    '[data-test-id="folders/labels:item-type:label"] [data-test-id="folders/labels:item-edit"]'
  )
    .first()
    .scrollIntoView()
    .click();
  cy.get('[data-test-id="label/folder-modal:name"]')
    .clear()
    .type(new_label_name);
  cy.get('.field-container > [data-testid="dropdown-button"]').click();
  cy.get(".color-selector-item").last().click();
  cy.get(".modal-two-footer > .button-solid-norm").click();
  cy.get('[data-test-id="folders/labels:item-name"]')
    .scrollIntoView()
    .should("have.text", new_label_name);
});
